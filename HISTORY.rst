History
========
1.2.dev(2023-08-xx) (Papiris)
------------------

- Move to safer xml parsing
- Add some onvif messages
- Add handling of non-conformant onvif devices


1.1 (2020-04-16)
-----------------

- Improved command-line client
- Major refactoring

1.0 (2017-05-25)
-----------------

- First PyPI release (petri)
